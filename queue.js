let collection = [];

// Write the queue functions below.
function print() {
    return collection;
}

function enqueue(names) {
    collection.push(names);
    return collection;
    
}

function dequeue() {
    return collection.shift();
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}


module.exports = {

    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty

};